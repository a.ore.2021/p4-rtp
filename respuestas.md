## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: 1268 paquetes
* ¿Cuánto tiempo dura la captura?: 12.810068 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 100 paquetes/segundo
* ¿Qué protocolos de nivel de red aparecen en la captura?: IPv4 (Interner Protocol Version 4), solo emplean este en toda la captura.
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP (User Datagram Protocol), solo emplean este en toda la captura.
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP (Real-time Transport Protocol), solo emplean este en toda la captura.
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 202
* Dirección IP de la máquina A: 216.234.64.16
* Dirección IP de la máquina B: 192.168.0.10
* Puerto UDP desde el que se envía el paquete: 54550
* Puerto UDP hacia el que se envía el paquete: 49154
* SSRC del paquete: 0x31be1e0e (834543118)
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): ITU-T G.711 PCMU
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 5
* ¿Cuántos paquetes hay en el flujo F?: 642 paquetes
* ¿El origen del flujo F es la máquina A o B?: La máquina origen del flujo F es la máquina B (192.168.0.10)
* ¿Cuál es el puerto UDP de destino del flujo F?: El puerto UDP de destino del flujo F es 54550
* ¿Cuántos segundos dura el flujo F?: 12.81 segundos
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 31.653 ms
* ¿Cuál es el jitter medio del flujo?: 12.2342 ms 
* ¿Cuántos paquetes del flujo se han perdido?: 0 (0.0%)
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: Está aproximadamente 12.78 segundos (12.81-0.03)
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 26630
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: Ha llegado pronto ya que el valor del "Skew" es 0.3920 
* ¿Qué jitter se ha calculado para ese paquete?: 12.483477 ms
* ¿Qué timestamp tiene ese paquete?: 16320
* ¿Por qué número hexadecimal empieza sus datos de audio?: f
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: vie 20 oct 2023 18:45:18 CEST
* Número total de paquetes en la captura: 4980 paquetes
* Duración total de la captura (en segundos): 14.0567 segundos
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: 450 paquetes
* ¿Cuál es el SSRC del flujo?: 0xbab31198
* ¿En qué momento (en ms) de la traza comienza el flujo? 8294 ms
* ¿Qué número de secuencia tiene el primer paquete del flujo? 23247
* ¿Cuál es el jitter medio del flujo?: 0.00
* ¿Cuántos paquetes del flujo se han perdido?: 0 (0.0 %)
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?: David Santa Cruz del Moral
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura): Su captura tiene 2125 paquetes de menos que la mia
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?: Tiene el mismo número de paquetes en el flujo RTP que yo (450 paquetes)
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?: El número de secuencia de su primer paquete RTP es 1001 y el mio es 23247. Por lo que hay una diferencia de 22246.
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?: Las principales diferencias son => el valor de los campos, SSRC, el se las deltas (ms), el Start Time y el ancho de banda.
